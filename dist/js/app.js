'use strict';

/*
*  Events
*/
var ALL_FILE_EVENTS = 'file';
var FILE_ADDED_EVENT = 'file.added';
var FILE_DELETE_EVENT = 'file.delete';
var FILES_CHANGED_EVENT = 'file.all.changed';
var FILES_SAVE_EVENT = 'file.all.save';
var FILES_SAVED_EVENT = 'file.all.saved';

/*
*  Agenda
*/
var agenda = {
  // DOM CACHE
  filesListWrapper: document.querySelector('#agenda-files-list'),
  saveFileButton: document.querySelector('[data-js-trigger="agenda-files-save"]'),
  // LOCAL AGENDA STATE
  // this file is here for demo purposes as if it was an old file
  files: {
    '3123-13-13123123213-23123123': 'admin-old-file.jpg'
  },
  // Dummy items state
  items: [{
    order: '1',
    title: 'Admin',
    files: ['3123-13-13123123213-23123123']
  }, {
    order: '2',
    title: 'Minutes Last Meeting',
    files: []
  }, {
    order: '3',
    title: 'Performance / Key Initiatives',
    files: []
  }, {
    order: '3.1',
    title: 'Germany',
    files: []
  }, {
    order: '3.2',
    title: 'France',
    files: []
  }],

  // Call agenda.init() outside to start the agenda
  init: function init() {
    if (Object.keys(this.files).length > 0) {
      this.renderFiles();
    }
    // ALL_FILE_EVENTS represents all events in the FILE namespace
    PubSub.subscribe(ALL_FILE_EVENTS, this.router.bind(this));

    // onClick - Publish event for save button
    this.saveFileButton.addEventListener('click', function () {
      return PubSub.publish(FILES_SAVE_EVENT);
    });
  },


  // A small router function to call the correct functions
  // depending on which event was triggered
  router: function router(msg, data) {
    if (msg === FILE_ADDED_EVENT) this.addFile(data);
    if (msg === FILE_DELETE_EVENT) this.removeFile(data);
    if (msg === FILES_CHANGED_EVENT) this.renderFiles();
    if (msg === FILES_SAVE_EVENT) this.saveFiles();
    if (msg === FILES_SAVED_EVENT) outputDummyData(data);
  },


  /** ------------
   * STATE Mutation Functions
  ------------ **/

  // file should be an object { id: HASH, filename: 'example.png' }
  addFile: function addFile(file) {
    if (!file) {
      return;
    }

    this.files[file.id] = file.filename;
    PubSub.publish(FILES_CHANGED_EVENT, this.files);
  },
  removeFile: function removeFile(id) {
    if (!id) {
      return;
    }

    // Logic for removing file from upload list here
    if (!!this.files[id]) {
      delete this.files[id];
      PubSub.publish(FILES_CHANGED_EVENT, this.files);
    }
  },
  saveFiles: function saveFiles() {
    var fileSelectsArray = Object.values(document.querySelectorAll('[data-js-trigger="agenda-file-select"]'));
    // loop through all agenda items
    this.items.forEach(function (item) {
      // we check if any files belong to this item and populate the state
      item.files = fileSelectsArray.filter(function (select) {
        return select.value == item.order;
      }).map(function (select) {
        return select.id;
      });
    });

    PubSub.publish(FILES_SAVED_EVENT, { items: this.items, files: this.files });
  },


  /** ------------
  * Render Functions
  ------------ **/

  renderFiles: function renderFiles() {
    var _this = this;

    var html = '';
    Object.keys(this.files).forEach(function (key) {
      html += _this.getFileItemHtml({
        id: key,
        filename: _this.files[key]
      });
    });
    this.filesListWrapper.innerHTML = html;
  },


  /** ------------
  * Util Functions
  ------------ **/

  /**
  * Returns a file extension
  * @param {String} filename 'image.png'
  * @return {String} jpg|jpeg|png|pdf|...|
  **/
  getFileExtension: function getFileExtension(filename) {
    if (!filename) {
      return '';
    }
    return filename.split('.').pop();
  },


  /**
  * Returns filename without extension
  * @param {String} filename 'example-name.png'
  * @return {String} 'example-name'
  **/
  getFilenameWithoutExtension: function getFilenameWithoutExtension(filename) {
    if (!filename) {
      return '';
    }
    return filename.replace(/\.[^/.]+$/, "");
  },


  /**
  * Returns a class of icon (font-awesome) to be used
  * with this type of file extension
  * @param {String} extension 'png'
  * @return {String} 'fa-file-o'
  **/
  getFileIconClass: function getFileIconClass(extension) {
    if (!extension) {
      return fileClass;
    }

    var fileClass = 'fa-file-o';

    if (extension == 'jpg' || extension == 'jpeg' || extension == 'png' || extension == 'svg') {
      fileClass = 'fa-file-image-o';
    }

    if (extension == 'word' || extension == 'doc' || extension == 'docx' || extension == 'txt') {
      fileClass = 'fa-file-text-o';
    }

    if (extension == 'pdf') {
      fileClass = 'fa-file-pdf-o';
    }

    return fileClass;
  },


  /**
  * Get The agenda item order number that relates more to file
  * @param {String} id HASH
  * @return {String} agenda item order number
  **/
  getRelatedAgendaItem: function getRelatedAgendaItem(fileId) {
    if (!fileId || this.items.length == 0) {
      return null;
    }

    var filename = this.getFilenameWithoutExtension(this.files[fileId]);

    // Check if this fileId is already inside any agenda.item.file list
    var currentFileParent = this.items.find(function (item) {
      return item.files.includes(fileId);
    });
    if (currentFileParent) {
      // if it is, then the related item is the current one
      return currentFileParent.order;
    }

    var orderOptions = this.items.map(function (item) {
      return item.order;
    });
    // Check for number matching filename i.e. '2.3' or '3' would match
    var numbersMatches = filename.match(/(?:\d*\.)?\d+/);
    if (numbersMatches && orderOptions.includes(numbersMatches[0])) {
      // we can return the match since it is the same as the agenda order
      return numbersMatches[0];
    }

    // Check for text matching filename
    var textMatch = this.items.find(function (item) {
      // split spaces and special characters from filename
      var splittedFilename = filename.toLowerCase().replace(/[^A-Z0-9]+/ig, '_').split('_');
      var foundMatch = false;

      splittedFilename.forEach(function (str) {
        // check if filename is substring of agenda item title and vice versa
        if (item.title.toLowerCase().includes(str) || str.includes(item.title.toLowerCase())) {
          foundMatch = true;
        }
      });
      // return if we found a text match or not
      return foundMatch;
    });

    // If a text match was found, we return the order of that agenda item
    if (textMatch) {
      return textMatch.order;
    }

    // If no relation is found, we return the first agenda item
    return orderOptions[0];
  },


  /** ------------
  * HTML Template Functions
  ------------ **/
  /**
  * Returns HTML for the html select
  * @param {String} id
  * @param {String} selectedOption
  * @return {String} html
  **/
  getSelectOptionHtml: function getSelectOptionHtml(id, selectedOption) {
    var html = '<div class="select-wrapper">';

    html += '<select id="' + id + '" data-js-trigger="agenda-file-select">';
    this.items.map(function (item) {
      var selected = selectedOption == item.order ? 'selected' : '';
      var subStringLabel = item.title.substr(0, 8) + '...';
      html += '<option ' + selected + ' value="' + item.order + '">' + item.order + ' ' + subStringLabel + '</option>';
    });
    html += '</select>';
    html += '<i class="fa fa-chevron-down"></i>';
    html += '</div>';

    return html;
  },


  /**
  * Returns HTML for the file item
  * @param {String} id
  * @param {String} filename
  * @return {String} html
  **/
  getFileItemHtml: function getFileItemHtml(_ref) {
    var id = _ref.id,
        filename = _ref.filename;

    var nodeClass = 'action-modal__files-list__item';
    var filenameWithoutExtension = this.getFilenameWithoutExtension(filename);
    var displayFilename = filenameWithoutExtension.length > 17 ? filename.substr(0, 15) + '..' : filenameWithoutExtension;
    var fileExtension = this.getFileExtension(filename);
    var fileIconClass = this.getFileIconClass(fileExtension);
    var relatedAgendaItem = this.getRelatedAgendaItem(id);

    var html = '<li class="' + nodeClass + '" data-file-item-id="' + id + '">';
    html += '<span class="icon"><i class="fa ' + fileIconClass + '"></i></span>';
    html += '<div class="' + nodeClass + '__details">';
    html += '<span>' + displayFilename + '</span>';
    if (fileExtension) {
      html += '<span>.' + fileExtension + '</span>';
    }
    html += '</div>';
    html += '<div class="' + nodeClass + '__actions">';
    html += '<span><i>belongs to</i></span>';
    html += this.getSelectOptionHtml(id, relatedAgendaItem);
    html += '<a href="#" data-js-trigger="remove-upload-file" onClick="PubSub.publish(\'' + FILE_DELETE_EVENT + '\', \'' + id + '\')" class="icon icon--small"><i class="fa fa-trash"></i></a>';
    html += '</div>';
    html += '</li>';

    return html;
  }
};

agenda.init();

/*
*  Dummy state output function
*/
var outputDummyData = function outputDummyData(_ref2) {
  var items = _ref2.items,
      files = _ref2.files;

  alert('Files saved, please check the console for the output');

  console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
  console.log('-------------------------------');
  console.log('THE CURRENT STATE OF THE AGENDA');
  console.log('-------------------------------');
  items.forEach(function (item) {
    var fileNames = '';
    item.files.forEach(function (fileId) {
      return fileNames = fileNames + ' [' + files[fileId] + ']';
    });

    if (fileNames.length == 0) {
      fileNames = 'none';
    }
    console.log('[' + item.order + ' - ' + item.title + '] saved with: ' + fileNames);
  });
  console.log('-------------------------------');
};

/*
*  Dropzone
*/
var myDropzone = new Dropzone('#my-dropzone', { url: '/fake/upload' });
/*
*  Dropzone offers a 'addedfile' event which i'm using to simulate
*  an upload success event
*/
myDropzone.on('addedfile', function (file) {
  PubSub.publish(FILE_ADDED_EVENT, { id: file.upload.uuid, filename: file.name });
});