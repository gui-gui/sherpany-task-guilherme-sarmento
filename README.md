# README #

### What is this repository for? ###

This is a task i've done to apply for a position as Front-End Developer @ Sherpany

### How do I get set up? ###

* Download the files or clone the repo
* Open index.html inside a browser

If you want to build the dist files yourself again

```
cd path/to/sherpany-task
npm install
gulp
```

### My notes for the reviewer ###

Please, review the code inside `index.html` and the `src` folder. The `dist` folder is built using `gulp`.

If possible, try to use the files inside `files` folder to test the upload and name suggestion functionalities.

I've made an effort to bring the upload > guessing the agenda item > user input > save work as smoothly as possible. Some other functions througout the prototype will not be working, like hiding the modal, or the agenda list updating with the new saved files, this is because i felt these were not the main focus of this task.

You will notice that i am using font-awesome icons througout the prototype, this is simply for the sake of simplicity, and does not mean i would be using these icons in the final version.

The UX/UI solution i came up with is fairly simple and straight forward, each uploaded file gets a `select` where the user can select the related agenda item. The `select` comes pre-populated with a guess for which agenda item that file might be related to.

Here is the check each file in the list goes through. 
If one of these condition are met, the option related to this agenda item is selected.

1. Does this file already belong to an agenda item?
2. Does this file contain a number related to an agenda item? 
3. Does the filename contain any words that match the agenda item title or vice versa?
4. If no matches in the first 3 checks then we match agenda item order = 1


#### File structure and code decisions####

#### CSS ####

I've made an effort to emulate the design and layout i found in the screenshot. 
I did not have access to the actual working version, because i did not have a login, but i think i got pretty close :).

I'm using scss to write my css using the BEM naming convention:

The `src/sass/` contains all the files that are then compiled with `gulp` to `dist/css`.

#### JS ####

I decided to try doing this task using vanilla js, despite not being the most efficient way to do it, i had some good fun while trying it.

The biggest pain point with using vanilla js is how to handle DOM manupulation and manage the app STATE without creating a mess. I love tools like Vue.js that do all of this stuff for us, or at least, make it pretty clear on what the good practice is. But having all the options in the world to chose from, i've decided to use a PUBSUB pattern and have a simple "componentized" `agenda` object that would wrap everything necessary to properly handle the events triggered.

I decided not to structure the files in more than one file, so all the javascript will be found inside `src/js/app.js`. I thought of using webpack or rollup to bundle it up and work with ES6 `imports` and `exports` and a more modular file structure, but the extra hassle to setup these tools for such a simple prototype seemed a bit too much.


### Conclusion ###

Unfortunetly due to time limitations i had to stop short of what i think would be an ideal solution. Because of that, I see a lot of room for improvements in my javascript, scss, and file structure, and would love to talk more about it if you guys feel i could be a good fit :)

### Contact Info ###

skype and email: guirams@gmail.com  
+55 21 971292563